﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCdemo.Classes;
using MVCdemo.Models;

namespace MVCdemo.Controllers
{
    public class PandaController : Controller
    {
        private readonly PandaContext _context;


        public PandaController(PandaContext context)
        {
            _context = context;
        }

        // GET: Panda
        public ActionResult Index()
        {
            var AllPandas = _context.Pandas.ToList();
            PandaViewModel vmodel = new PandaViewModel(){Pandas = AllPandas};
            vmodel.Pandas.Add(new PandaModel(){Id = 999, Name = "Micky", PhotoUrl = "https://images.csmonitor.com/csm/2017/03/1030444_1_0305-panda_standard.jpg?alias=standard_900x600nc" });
            return View(vmodel);
        }

        // GET: Panda/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Panda/Create
        public ActionResult Create()
        {


            return View();
        }

        // POST: Panda/Create
        [HttpPost]
        public ActionResult Create(PandaModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Pandas.Add(model);
                    _context.SaveChanges();
               
                    return RedirectToAction("Index");
                }
                catch
                {
                    return View();
                }
            }

            ViewBag.error = ModelState;
            return View();
        }

        // GET: Panda/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Panda/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Panda/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Panda/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
