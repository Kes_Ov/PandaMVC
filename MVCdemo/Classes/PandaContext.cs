﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MVCdemo.Models;

namespace MVCdemo.Classes
{
    public class PandaContext : DbContext
    {
        public PandaContext()
            : base("DefaultConnection")
        {}

        public DbSet<PandaModel> Pandas { get; set; }
    }
}