﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVCdemo.Models;

namespace MVCdemo.Models
{
    public class PandaViewModel
    {
        public List<PandaModel> Pandas { get; set; }
    }
}